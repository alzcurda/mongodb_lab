package part_c;

import com.mongodb.client.result.UpdateResult;
import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;

import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.person.Person;

import java.util.Arrays;

public class Part_C_model_2 {
	public static void dataGenerator(int N) {
		Fairy fairy = Fairy.create();

		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("test");
		MongoCollection<Document> cPerson = database.getCollection("m2");

		// Delete collection to avoid repeated documents.
		cPerson.drop();

		Person person;
		Document dPerson;
		Document dCompany;

		for (int i = 0; i < N; ++i) {
			//new person
			person = fairy.person();

			dPerson = new Document();
			dPerson.put("_id",person.getPassportNumber());
			dPerson.put("firstName", person.getFirstName());
			dPerson.put("fullName", person.getFullName());
			dPerson.put("passportNumber", person.getPassportNumber());
			dPerson.put("dateOfBirth", String.valueOf(person.getDateOfBirth()));
			dPerson.put("age", String.valueOf(person.getAge()));



			dCompany = new Document();
			dCompany.put("vatIdentificationNumber", person.getCompany().getVatIdentificationNumber());
			dCompany.put("company_name", person.getCompany().getName());
			dPerson.put("company", dCompany);

			cPerson.insertOne(dPerson);


		}

		//*************************************************************************************************************
		// Q1 For each person, retrieve their full name and their company's name.
		//*************************************************************************************************************
		Document query = new Document();
		Document projection = new Document();
		projection.put("fullname",1);
		projection.put("company.company_name",1);

		long startTime = System.currentTimeMillis(); // Get time at the start of the query
		String queryResult = cPerson.find(query).projection(projection).first().toJson();
		long queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q1[ " + queryTime + " ms]: " + queryResult);

		//*************************************************************************************************************
		//Q2 For each company, retrieve its name and the number of employees.
		//*************************************************************************************************************

		startTime = System.currentTimeMillis(); // Get time at the start of the query
		//Foreach company we obtain their persons and count them
		queryResult = cPerson.aggregate(Arrays.asList(
				Aggregates.unwind("$company"),
				Aggregates.group("$company.vatIdentificationNumber", Accumulators.sum("number_employees", 1))
				)
		).first().toJson();

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q2[ " + queryTime + " ms]: " + queryResult);

		//*************************************************************************************************************
		//Q3 For each person born before 1988, update their age to \30".
		//*************************************************************************************************************

		Document selectQuery = new Document();

		selectQuery.put("dateOfBirth", new Document("$lt", "1988-12-31T23:59:59.978+02:00"));

		Document updateFields = new Document();
		updateFields.put("age","30");

		Document updateObject  = new Document();
		updateObject.put("$set",updateFields);

		startTime = System.currentTimeMillis(); // Get time at the start of the query
		//Foreach company we obtain their persons and count them

		UpdateResult uResult = cPerson.updateMany(selectQuery,updateObject);

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q3[ " + queryTime + " ms]: " + uResult.toString());



		//*************************************************************************************************************
		//Q4 For each company, update its name to include the word "Company"
		//*************************************************************************************************************
		startTime = System.currentTimeMillis(); // Get time at the start of the query
		cPerson.aggregate(Arrays.asList(new Document("$addFields",
						new Document("company.company_name",
								new Document("$concat", Arrays.asList("$company.company_name", " ", "Company"))))
				,new Document("$out","m2")
		))
				.toCollection();
		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q.4[ " + queryTime + " ms]: ");

		/* old version
		startTime = System.currentTimeMillis(); // Get time at the start of the query
		Document nCompany;
		for (Document document : cPerson.find()) {
			selectQuery = new Document();
			selectQuery.put("passportNumber", document.get("passportNumber"));
			//First we return company nested document, I tried to get company.company_name but it returned null.

			nCompany = document.get("company", Document.class);

			updateFields = new Document();
			updateFields.put("company.company_name", nCompany.get("company_name")+" Company");

			updateObject  = new Document();
			updateObject.put("$set",updateFields);
			cPerson.updateOne(selectQuery,updateObject);
		}

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q4[ " + queryTime + " ms]: ");

		 */

		client.close();
	}
}
