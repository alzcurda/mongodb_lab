package part_c;

import com.mongodb.client.result.UpdateResult;
import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;

import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.person.Person;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Set;

// Prueba versionado

public class Part_C_model_1 {
	//Nested Class Company
	public static class Company {
		String id;
		String company_name;
		public Company(String id, String company_name){
			this.id = id;
			this.company_name = company_name;
		}
	}
	public static void dataGenerator(int N) {
		Fairy fairy = Fairy.create();

		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("test");
		MongoCollection<Document> cPerson = database.getCollection("person");
		MongoCollection<Document> cCompany = database.getCollection("company");
		// Delete collection to avoid repeated documents.
		cPerson.drop();
		cCompany.drop();

		Person person;
		Document dPerson;
		Document dCompany;
		//Random function generates duplicated company keys. To solve this we save different companies in a Hastable.
		//We wanted to use a Hashlist, but random function generates same key with different attributes
		Hashtable<String,Company> hCompany = new Hashtable<>();
		Company vCompany;
		String companyId;
		for (int i = 0; i < N; ++i) {
			//new person
			person = fairy.person();

			dPerson = new Document();
			dPerson.put("_id",person.getPassportNumber());
			dPerson.put("firstName", person.getFirstName());
			dPerson.put("fullName", person.getFullName());
			dPerson.put("passportNumber", person.getPassportNumber());
			dPerson.put("dateOfBirth", String.valueOf(person.getDateOfBirth()));
			dPerson.put("age", String.valueOf(person.getAge()));
			companyId = person.getCompany().getVatIdentificationNumber();
			dPerson.put("companyId", companyId);

			cPerson.insertOne(dPerson);

			//update unique list of companies
			vCompany = new Company(companyId,person.getCompany().getName());
			hCompany.put(companyId,vCompany);
		}
		//Create new company for each company in the hashtable
		Set<String> keys = hCompany.keySet();
		Company iCompany;
		for(String key: keys){
			iCompany = hCompany.get(key);
			dCompany = new Document();
			dCompany.put("_id", iCompany.id);
			dCompany.put("vatIdentificationNumber", iCompany.id);
			dCompany.put("company_name", iCompany.company_name);
			cCompany.insertOne(dCompany);
		}

		//*************************************************************************************************************
		// Q1 For each person, retrieve their full name and their company's name.
		//*************************************************************************************************************
		long startTime = System.currentTimeMillis(); // Get time at the start of the query
		String queryResult = cPerson.aggregate(Arrays.asList(
				Aggregates.lookup("company","companyId","vatIdentificationNumber","company"),
				Aggregates.unwind("$company"),
				Aggregates.project(Projections.fields(Projections.include("fullName","company.company_name")))
				)
		).first().toJson();
		long queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q1[ " + queryTime + " ms]: " + queryResult);

		//*************************************************************************************************************
		//Q2 For each company, retrieve its name and the number of employees.
		//*************************************************************************************************************
		startTime = System.currentTimeMillis(); // Get time at the start of the query
		//Foreach company we obtain their persons and count them
		queryResult = cCompany.aggregate(Arrays.asList(
				Aggregates.lookup("person","vatIdentificationNumber","companyId","person"),
				Aggregates.unwind("$person"),
				Aggregates.group("$vatIdentificationNumber", Accumulators.sum("number_employees", 1))
				)
		).first().toJson();

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q2[ " + queryTime + " ms]: " + queryResult);

		//*************************************************************************************************************
		//Q3 For each person born before 1988, update their age to \30".
		//*************************************************************************************************************
		Document selectQuery = new Document();
		selectQuery.put("dateOfBirth", new Document("$lt", "1988-12-31T23:59:59.978+02:00"));

		Document updateFields = new Document();
		updateFields.put("age","30");

		Document updateObject  = new Document();
		updateObject.put("$set",updateFields);

		startTime = System.currentTimeMillis(); // Get time at the start of the query
		//Foreach company we obtain their persons and count them

		UpdateResult uResult = cPerson.updateMany(selectQuery,updateObject);

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q3[ " + queryTime + " ms]: " + uResult.toString());


		/*
		I tried to update with this shell sentence:
		db.company.updateMany(
			{},
			[
				{$set: {"company_name": { "$concat": ["$company_name", " ", "Company"]}}}
			]
		)
		But in Java I wasn't been able to work it because we can't use $concat in an update sentente.
		This command is better than the final solution because we update directly entire collection
		 */

		//*************************************************************************************************************
		//Q4 For each company, update its name to include the word "Company"
		//*************************************************************************************************************

		startTime = System.currentTimeMillis(); // Get time at the start of the query
		//We concatenate " Company" at the end of company_name
		cCompany.aggregate(Arrays.asList(new Document("$addFields",
				new Document("company_name",
					new Document("$concat", Arrays.asList("$company_name", " ", "Company"))))
				,new Document("$out","company")
				))
				.toCollection();

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q.4.2[ " + queryTime + " ms]: ");

		/* Foreach company we obtain their persons and count them
		startTime = System.currentTimeMillis(); // Get time at the start of the query
		for (Document document : cCompany.find()) {
			selectQuery = new Document();
			selectQuery.put("vatIdentificationNumber", document.get("vatIdentificationNumber"));

			updateFields = new Document();
			updateFields.put("company_name", document.get("company_name")+" Company");

			updateObject  = new Document();
			updateObject.put("$set",updateFields);
			cCompany.updateOne(selectQuery,updateObject);

		}
		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time
		System.out.println("Q.4[ " + queryTime + " ms]: ");
		 */
		client.close();
	}
}
