package part_c;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;

import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.person.Person;
import io.codearte.jfairy.producer.company.Company;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Part_C_model_3 {
	public static void dataGenerator(int N) {
		Fairy fairy = Fairy.create();

		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("test");
		MongoCollection<Document> cCompany = database.getCollection("m3");

		// Delete collection to avoid repeated documents.
		cCompany.drop();

		Person person;
		Company company;
		Document dPerson;
		List<Document> dPersons;
		Document dCompany;
		int numPerXComp = 100;
		//Generate 1 company for 100 employes.
		double d = ((double)N)/numPerXComp;
		int nc = (int) Math.ceil(d);
		for (int i = 0; i < nc; ++i) {
			company = fairy.company();
			dCompany = new Document();
			dPersons = new ArrayList<>();
			dCompany.put("_id",company.getVatIdentificationNumber());
			dCompany.put("vatIdentificationNumber", company.getVatIdentificationNumber());
			dCompany.put("company_name", company.getName());

			for (int j = 0; j < numPerXComp; ++j) {
				//new person
				person = fairy.person();

				dPerson = new Document();
				dPerson.put("firstName", person.getFirstName());
				dPerson.put("fullName", person.getFullName());
				dPerson.put("passportNumber", person.getPassportNumber());
				dPerson.put("dateOfBirth", String.valueOf(person.getDateOfBirth()));
				dPerson.put("age", String.valueOf(person.getAge()));
				//We put in a list each person created.
				dPersons.add(dPerson);
			}
			dCompany.put("person", dPersons);
			cCompany.insertOne(dCompany);
		}


		//*************************************************************************************************************
		// Q1 For each person, retrieve their full name and their company's name.
		//*************************************************************************************************************

		long startTime = System.currentTimeMillis(); // Get time at the start of the query
		String queryResult = cCompany.aggregate(Arrays.asList(
				Aggregates.unwind("$person"),
				Aggregates.project(Projections.fields(Projections.include("person.fullName","company_name"),Projections.exclude("_id")))
				)
		).first().toJson();

		/* Test all results
		AggregateIterable a = cCompany.aggregate(Arrays.asList(
				Aggregates.unwind("$person"),
				Aggregates.project(Projections.fields(Projections.include("person.fullName","company_name")))
				)
		);
		for (MongoCursor it = a.iterator(); it.hasNext(); ) {
			Document doc = (Document) it.next();
			System.out.println("doc "+doc);
		}
		 */

		long queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q1[ " + queryTime + " ms]: " + queryResult);

		//*************************************************************************************************************
		//Q2 For each company, retrieve its name and the number of employees.
		//*************************************************************************************************************

		startTime = System.currentTimeMillis(); // Get time at the start of the query
		//Foreach company we obtain their persons and count them
		queryResult = cCompany.aggregate(Arrays.asList(
				Aggregates.unwind("$person"),
				Aggregates.group("$vatIdentificationNumber", Accumulators.sum("number_employees", 1))
				)
		).first().toJson();

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q2[ " + queryTime + " ms]: " + queryResult);

		//*************************************************************************************************************
		//Q3 For each person born before 1988, update their age to \30".
		//*************************************************************************************************************
		/*
		startTime = System.currentTimeMillis(); // Get time at the start of the query

		//First, we get all persons to update
		Document selectQuery = new Document();

		selectQuery.put("person.dateOfBirth", new Document("$lt", "1988-12-31T23:59:59.978+02:00"));

		AggregateIterable personToUpdate = cCompany.aggregate(Arrays.asList(
				Aggregates.unwind("$person"),
				Aggregates.match(selectQuery),
				Aggregates.project(Projections.fields(Projections.exclude("_id")))
				)
		);

		//Second, we update each person, one to one, with the correct age.
		Document updateFields = new Document();
		updateFields.put("person.$.age","30");

		Document updateObject  = new Document();
		updateObject.put("$set",updateFields);

		Document updateQuery;
		Document nPerson;

		for (MongoCursor it = personToUpdate.iterator(); it.hasNext(); ) {
			Document document = (Document) it.next();
			nPerson = document.get("person", Document.class);
			updateQuery = new Document()
					.append("person.passportNumber", nPerson.get("passportNumber"));
			cCompany.updateOne(updateQuery,updateObject, new UpdateOptions());
		}

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q3[ " + queryTime + " ms]: ");
		*/

		startTime = System.currentTimeMillis(); // Get time at the start of the query
		cCompany.aggregate(Arrays.asList(
				new Document("$match",
						new Document("person.dateOfBirth",
								new Document("$lt", "1988-12-31T23:59:59.978+02:00"))),
				new Document("$set",
						new Document("person.age", "30"))
				,new Document("$out","m3"))).toCollection();

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q3.2[ " + queryTime + " ms]: ");
		//*************************************************************************************************************
		//Q4 For each company, update its name to include the word "Company"
		//*************************************************************************************************************

		startTime = System.currentTimeMillis(); // Get time at the start of the query
		//We concatenate " Company" at the end of company_name
		cCompany.aggregate(Arrays.asList(new Document("$addFields",
						new Document("company_name",
								new Document("$concat", Arrays.asList("$company_name", " ", "Company"))))
				,new Document("$out","company")
		))
				.toCollection();

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q.4.2[ " + queryTime + " ms]: ");

		/*
		startTime = System.currentTimeMillis(); // Get time at the start of the query

		//Foreach company we obtain their persons and count them
		for (Document document : cCompany.find()) {
			selectQuery = new Document();
			selectQuery.put("vatIdentificationNumber", document.get("vatIdentificationNumber"));

			updateFields = new Document().append("company_name", document.get("company_name")+" Company");

			updateObject  = new Document().append("$set",updateFields);

			cCompany.updateOne(selectQuery,updateObject);
		}

		queryTime = System.currentTimeMillis() - startTime; // Measure query execution time

		System.out.println("Q4[ " + queryTime + " ms]: ");
		*/

		client.close();
	}
}
