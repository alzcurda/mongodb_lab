cd c:\Program Files\MongoDB\Server\4.4\bin
mongo
--4.2.1
--Q1
db.restaurants.insertOne(
	{
	"address": {
	"coord": [2.136236, 41.387950],
	"street": "Constanca, 8",
	"zipcode": "08029"
	},
	"borough": "Eixample",
	"cuisine": "Catalan",
	"name": "La Llimona",
	"restaurant_id": "00000000"
	}
)

--Q2
db.restaurants.updateOne(
	{"restaurant_id": "00000000"},
	{$set: 
		{"borough": "Les Corts",
		"address.street": "Constanca, 12"
		}
	}
)

--Q3
db.restaurants.remove({"restaurant_id":"00000000"})

--4.2.2
--Q1
db.restaurants.find({borough:"Manhattan"})

--Q2: List all restaurants with at least one grade with value C.
db.restaurants.find({"grades.grade":'C'})

--Q3: Get the number of restaurants in \Manhattan" with some grade scored greater than 10.
db.restaurants.find({"borough":"Manhattan","grades.score": {"$gt":10}}).count()

--Q4: Get the average score by cuisine type.
db.restaurants.aggregate(
	[
		 {$unwind: "$grades" },
		 {$group:
			{_id:"$cuisine", 
			 "avgScore": { "$avg": "$grades.score" }
			}
		}
	]
)

--Q5: List the number of diferent cuisine types for each borough.
db.restaurants.aggregate(
	[
		 {$group:{_id:{"borough":"$borough","cuisine":"$cuisine"}}},
		 {$group:{_id:"$_id.borough",
		 "difCuisines": {"$sum": 1}}}
	]
)

db.restaurants.aggregate([
	{$group: { _id: "$borough" , distinctValues: { $addToSet: "$cuisine"} }},
	{$project:{borough:1,dValues:{$size:"$distinctValues"}}}
])

--Q6: List restaurants close (150 meters or less) to the restaurant "The Assembly Bar".
db.restaurants.createIndex( { "address.coord" : "2dsphere" } )

var assembly = db.restaurants.findOne({name:"The Assembly Bar"})

db.restaurants.find(
{
   "address.coord": {
		$near: {
			$geometry: {
				type: "Point" ,
				coordinates: assembly.address.coord
				},
				$maxDistance: 150,
				$minDistance: 0
		}
   }
}
)

