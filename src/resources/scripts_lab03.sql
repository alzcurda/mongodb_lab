cd c:\Program Files\MongoDB\Server\4.4\bin
mongo

--Q1 For each administrative region, the number of buildings that are within its coordinates.
geowithin

db.admin.createIndex( { "geometry" : "2dsphere" } )
db.buildings.createIndex( { "geometry" : "2dsphere" } )

var cursor = db.admin.find({})
cursor.forEach(function(admin) {
	print("Region administrativa: "+ admin.properties.name);
	var numberBuildings = db.buildings.count(
	{
		"geometry": {
				$geoWithin: {
					$geometry: admin.geometry
				}
		}
	}
	);
	print("Edificio: "+ numberBuildings);
});

--Q2: Roads that span along 5 or more administrative regions.
geointersects

db.roads.createIndex( { "geometry" : "2dsphere" } )

var c2 = db.roads.find({})

c2.forEach(function(roads) {	
	var numberSpan = db.admin.count(
	{
		"geometry": {
				$geoIntersects: {
					$geometry: roads.geometry
				}
		}
	}
	);
	if (numberSpan > 4){
		print("Road: "+ roads.properties.name + " number: " + numberSpan);
	} 	
});

--Q3: Ranking of most common amenity types in "Badalona"
geowithin y count?

db.amenities.createIndex( { "geometry" : "2dsphere" } )

var badalona = db.admin.findOne({"properties.name":"Badalona"})

print("Region administrativa: "+ badalona.properties.name);

var vAmenities = db.amenities.find(
		{
			"geometry": {
					$geoWithin: {
						$geometry: badalona.geometry
					}
			}
		}
);

print(vAmenities);

db.amenities.aggregate([
{$match: 
		{
			"geometry": {
					$geoWithin: {
						$geometry: badalona.geometry
					}
			}
		}	
}
,{$group:{_id:"$properties.type", "rank": {"$sum":1}}}
,{$project:{"properties.type":1,"rank":1}}
,{$sort:{"rank":-1}}
]);